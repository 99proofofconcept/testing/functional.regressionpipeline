import pytest
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


def pytest_addoption(parser):
    parser.addoption(
        "--browser", action="store", default="", help="my option: chrome or firefox"
    )


@pytest.fixture(scope="session", autouse=True)
def setup_session_browser(request):
    browser_from_terminal = request.config.getoption("--browser")
    browser_path_installation = __get_path_browser_installation(browser_from_terminal)

    request.session.browser_from_terminal = browser_from_terminal
    request.session.browser_path_installation = browser_path_installation


def __get_path_browser_installation(browser: str) -> str:
    if browser == "firefox":
        return GeckoDriverManager().install()
    elif browser == "chrome":
        return ChromeDriverManager().install()
