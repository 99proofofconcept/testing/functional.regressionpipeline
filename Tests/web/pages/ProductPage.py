from selenium.webdriver.common.by import By


class ProductPage(object):
    logo_product = (By.CLASS_NAME, "header_secondary_container")

    def __init__(self, browser):
        self.browser = browser

    def is_logo_displayed(self) -> bool:
        logo_element = self.browser.find_element(*ProductPage.logo_product)
        return logo_element.is_displayed()


