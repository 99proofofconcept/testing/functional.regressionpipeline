from selenium import webdriver
from selenium.webdriver.common.by import By

from Tests.web.pages.ProductPage import ProductPage


class LoginPage(object):
    user_name = (By.ID, "user-name")
    password = (By.ID, "password")
    login_button = (By.ID, "login-button")
    error_message = (By.CLASS_NAME, "error-button")

    def __init__(self, browser: webdriver):
        self.browser = browser

    def write_user_name(self, user_name: str):
        user_name_element = self.browser.find_element(*LoginPage.user_name)
        user_name_element.send_keys(user_name)

    def write_password(self, password: str):
        password_element = self.browser.find_element(*LoginPage.password)
        password_element.send_keys(password)

    def click_login_button(self):
        login_button_element = self.browser.find_element(*LoginPage.login_button)
        login_button_element.click()

    def login(self, username: str, password: str) -> ProductPage:
        self.write_user_name(username)
        self.write_password(password)
        self.click_login_button()

        return ProductPage(self.browser)

    def error_message_is_visible(self) -> bool:
        error_message_element = self.browser.find_element(*LoginPage.error_message)
        return error_message_element.is_displayed()
