import configparser

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service

from Tests.web.browsers.Browser import Browser


class Firefox_Browser(Browser):

    def __init__(self):
        self.browser = None

    def go_to_url(self, url: str) -> webdriver:
        self.browser.get(url)
        return self.browser

    def dispose_browser(self):
        self.browser.close()
        self.browser.quit()

    def set_browser_configuration(self, browser_path: str):

        mode = self.__get_mode()
        options = self.__get_default_configuration(mode)

        firefox_service = Service(browser_path)

        self.browser = webdriver.Firefox(service=firefox_service,
                                         options =options)
        self.browser.implicitly_wait(10)
        self.browser.maximize_window()

    def __get_mode(self) -> str:
        config = configparser.ConfigParser()
        config.read("config.ini")

        return "--" + config["driver_configuration"]["Mode"]

    def __get_default_configuration(self, mode: str) -> Options:
        options = Options()
        options.add_argument(mode)
        options.add_argument("-foreground")

        return options
