import abc
from selenium import webdriver


class Browser(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def go_to_url(self, url: str) -> webdriver:
        return

    @abc.abstractmethod
    def set_browser_configuration(self, browser_path: str):
        pass
