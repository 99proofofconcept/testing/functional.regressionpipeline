import pytest
from unittest import TestCase

from Tests.web.TestBase import setup_class_web_test
from Tests.web.pages.LoginPage import LoginPage


@pytest.mark.webtest
@pytest.mark.usefixtures("setup_class_web_test")
class Test_Login_Page(TestCase):

    url_login_page = 'https://www.saucedemo.com/'

    def test_user_should_be_able_to_login_with_right_password(self):

        driver = self.browser.go_to_url(Test_Login_Page.url_login_page)

        user_name = "standard_user"
        password = "secret_sauce"
        login_page = LoginPage(driver)
        product_page = login_page.login(user_name, password)

        self.assertTrue(product_page.is_logo_displayed())

    def test_user_should_show_an_error_message_when_user_is_locked(self):

        driver = self.browser.go_to_url(Test_Login_Page.url_login_page)

        user_name = "locked_out_user";
        password = "secret_sauce"
        login_page = LoginPage(driver)
        login_page.write_user_name(user_name)
        login_page.write_password(password)
        login_page.click_login_button()

        self.assertTrue(login_page.error_message_is_visible())
