import pytest

from Tests.web.browsers import Browser
from Tests.web.browsers.Chrome_Browser import Chrome_Browser
from Tests.web.browsers.Firefox_Browser import Firefox_Browser


@pytest.fixture(scope="class")
def setup_class_web_test(request):

    request.cls.browser = get_browser(
        request.session.browser_from_terminal,
        request.session.browser_path_installation
    )

    yield request.cls.browser

    request.cls.browser.dispose_browser()


def get_browser(browser: str, browser_path: str) -> Browser:
    if browser == "firefox":
        firefox_instance = Firefox_Browser()
        firefox_instance.set_browser_configuration(browser_path)
        return firefox_instance
    elif browser == "chrome":
        chrome_instance = Chrome_Browser()
        chrome_instance.set_browser_configuration(browser_path)
        return chrome_instance
    else:
        pass
