import pytest
from unittest import TestCase

from Tests.api.TestBase import setup_class_api_test
from Tests.api.city_resource.components.CityResource import CityResource


@pytest.mark.integration
@pytest.mark.usefixtures("setup_class_api_test")
class Test_Integration_City(TestCase):

    def test_cities_payload_should_be_as_expected(self):

        city_resource = CityResource(self.http_client)
        payload = city_resource.get_all_cities()

        self.assertEqual(len(payload.data), 5, "Number of cities should be 5")
        self.assertEqual(payload.data[0].id, 3350606, "City's Id does not match")
