
from Tests.api.a_core.HttpClient import HttpClient
from Tests.api.city_resource.components.CityModel import CitiesDTO, City


class CityResource(object):

    __domain = "https://wft-geo-db.p.rapidapi.com/v1/geo"

    __headers = {
        'x-rapidapi-host': "wft-geo-db.p.rapidapi.com",
        'x-rapidapi-key': "a75605c288msh9c637f6c8ac523cp131f32jsn0d504afd7f8e"
    }

    def __init__(self, http_client: HttpClient):
        self.__http_client = http_client

    def get_status_code(self) -> int:

        http_resource = self.__domain + "/cities"
        response = self.__http_client.get(http_resource, self.__headers)
        return response.status_code

    def get_all_cities(self) -> CitiesDTO:
        http_resource = self.__domain + "/cities"
        response = self.__http_client.get(http_resource, self.__headers)
        city = City()
        return city.get_cities(response.json())
