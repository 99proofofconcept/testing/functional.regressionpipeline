from dataclasses import dataclass
from typing import List
from dacite import from_dict


@dataclass
class CityDTO:
    id: int
    wikiDataId: str
    type: str
    city: str
    name: str
    country: str
    countryCode: str
    region: str
    regionCode: str


@dataclass
class CitiesDTO:
    data: List[CityDTO]


class City:

    def get_cities(self, value) -> CitiesDTO:
        cities = from_dict(data_class=CitiesDTO, data=value)
        return cities
