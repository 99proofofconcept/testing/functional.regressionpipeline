import pytest
from unittest import TestCase

from Tests.api.TestBase import setup_class_api_test
from Tests.api.city_resource.components.CityResource import CityResource


@pytest.mark.sanity
@pytest.mark.usefixtures("setup_class_api_test")
class Test_Sanity_City(TestCase):

    def test_status_code_cities_should_be_200(self):

        city_resource = CityResource(self.http_client)
        status_code = city_resource.get_status_code()
        self.assertEqual(status_code, 200)
