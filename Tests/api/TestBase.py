import pytest

from Tests.api.a_core.CustomHttpClient import CustomHttpClient


@pytest.fixture(scope="class")
def setup_class_api_test(request):
    http_client = CustomHttpClient()
    request.cls.http_client = http_client
    yield request.cls.http_client
    request.cls.http_client = None
