import requests
from urllib3 import disable_warnings, exceptions

from Tests.api.a_core.HttpClient import HttpClient
from requests.exceptions import ProxyError


class CustomHttpClient(HttpClient):

    def __init__(self):
        self.__headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
        self.__session = requests.Session()
        disable_warnings(exceptions.InsecureRequestWarning)

    def get(self, endpoint, headers=None, proxy=False):

        if headers is None:
            headers = self.__headers

        try:
            if proxy:
                return self.__session.get(endpoint, headers=headers, proxies={'https': '127.0.0.1:8888'}, verify=False)
            else:
                return self.__session.get(endpoint, headers=headers)
        except ProxyError:
            pass
