from abc import ABC, abstractmethod


class HttpClient(ABC):

    @abstractmethod
    def get(self, endpoint, headers=None, proxy=False):
        return
