from dataclasses import dataclass


@dataclass
class PlanetDTO:
    name: str
    terrain: str
    films: dict


class Planet:

    def __init__(self, data: dict):
        self.data = data

    def from_dict(self) -> PlanetDTO:
        value = PlanetDTO(name=self.data["name"],
                          terrain=self.data["terrain"],
                          films=self.data["films"])
        return value
