
from Tests.api.a_core.HttpClient import HttpClient
from Tests.api.planet_resource.components.PlanetModel import PlanetDTO, Planet


class PlanetResource(object):

    __domain = "http://swapi.dev"

    def __init__(self, http_client: HttpClient):
        self.__http_client = http_client

    def get_status_code(self, planet_id: str) -> int:

        http_resource = self.__domain + "/api/planets/%s/" % planet_id
        response = self.__http_client.get(http_resource)
        return response.status_code

    def get_planet_by_id(self, planet_id: str) -> PlanetDTO:
        http_resource = self.__domain + "/api/planets/%s/" % planet_id
        response = self.__http_client.get(http_resource)
        planet = Planet(response.json())
        return planet.from_dict()
