import pytest
from unittest import TestCase

from Tests.api.TestBase import setup_class_api_test
from Tests.api.planet_resource.components.PlanetResource import PlanetResource


@pytest.mark.integration
@pytest.mark.usefixtures("setup_class_api_test")
class Test_Integration_Planet(TestCase):

    def test_first_planet_should_contains_as_expected(self):

        planet_id = "1"

        planet_resource = PlanetResource(self.http_client)

        planet = planet_resource.get_planet_by_id(planet_id)

        self.assertEqual(planet.name, "Tatooine")
        self.assertEqual(planet.terrain, "desert")
        self.assertEqual(len(planet.films), 5)




