import pytest
from unittest import TestCase

from Tests.api.TestBase import setup_class_api_test
from Tests.api.planet_resource.components.PlanetResource import PlanetResource


@pytest.mark.sanity
@pytest.mark.usefixtures("setup_class_api_test")
class Test_Sanity_Planet(TestCase):

    def test_status_code_planet_should_be_200(self):

        planet_id = "1"

        planet_resource = PlanetResource(self.http_client)

        status_code = planet_resource.get_status_code(planet_id)

        self.assertEqual(status_code, 200)
