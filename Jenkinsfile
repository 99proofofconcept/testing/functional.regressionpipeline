pipeline {

    options {
        buildDiscarder(logRotator(numToKeepStr: '10')) 
        ansiColor('xterm')
        timestamps()
        skipDefaultCheckout(true)
    }

    agent any

    stages {

        stage('Checkout') {
           steps {
                cleanWs()
                checkout scm
            }
        }

        stage('Setup'){
            
            options{
                timeout(time: 5, unit: "MINUTES")
            }

            steps {
                script {
                    bat label: '', script: 'pip install -r requirements.txt'
                }
            }
        }

        stage('Run sanity tests') {

            options{
                timeout(time: 3, unit: "MINUTES")
            }

            steps {
                 bat label: '', script: ' pytest -v -m sanity --html=html/sanity.html --junit-xml=reports/sanity_tests.xml'
            }
        }

        stage('Run integration tests') {

            options{
                timeout(time: 5, unit: "MINUTES")
            }

            steps {
                bat label: '', script: ' pytest -v -m integration --html=html/integration.html --junit-xml=reports/integration_tests.xml'
            }
        }

         stage('Run web tests') {
            parallel {
                stage('Tests On Chrome') {

                    options{
                        timeout(time: 5, unit: "MINUTES")
                    }

                    steps {
                        bat label: '', script: ' pytest -q -s -m webtest --browser=chrome --html=html/web_chrome.html --junit-xml=reports/web_tests_chrome.xml'
                    }
                }
                stage('Tests On Firefox') {

                    options{
                        timeout(time: 5, unit: "MINUTES")
                    }

                    steps {
                        bat label: '', script: ' pytest -q -s -m webtest --browser=firefox --html=html/web_firefox.html --junit-xml=reports/web_tests_firefox.xml'
                    }
                }
            }
        }

        stage('Archive artifacts'){

            steps{
                archiveArtifacts artifacts: 'html/*.html, reports/*.xml', fingerprint: true
            }
        }
    }

    post {

        always {
            junit 'reports/*.xml'
        }

        failure {
            echo 'This will run only if failed'
        }
    }
}